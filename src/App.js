import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getChatLog, getChatMembers } from './service';
import MessageList from './Components/MessageList'
import './App.css';

export class App extends PureComponent {
  componentDidMount() {
    const { getChatLog, getChatMembers } = this.props;
    getChatLog();
    getChatMembers();
  }

  render() {
    const { messages, members } = this.props;
    
    return (
      <div>
        <h1>Messages</h1>
        {<MessageList messages={messages} members={members} />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages,
  members: state.members,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getChatLog, getChatMembers }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
