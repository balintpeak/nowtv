import React from 'react'

import Message from './Message'

function sortMessages(messages = []) {
  const sortedMessages = messages.sort((a, b) => {
    const dateA = new Date(a.timestamp);
    const dateB = new Date(b.timestamp);

    let result = 0;
    if (dateA.getTime() - dateB.getTime() < 0) {
      result = -1;
    } else if (dateA.getTime() - dateB.getTime() > 0) {
      result = 1;
    }

    return result;
  })

  return sortedMessages
}

const MessageList = ({ messages = [], members = [] }) => (
  <ul>
    {sortMessages(messages).map(message => <Message key={message.id} {...message} member={members[message.userId]} />)}
  </ul>
)

export default MessageList
