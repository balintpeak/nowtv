import React from 'react';
import ReactDOM from 'react-dom';
import MessageList from './MessageList';
import { shallow } from 'enzyme';

import Message from './Message';
jest.mock('./Message');
Message.mockImplementation(() => <div />);

describe('MessageList', () => {
  it('should render without crashing', () => {
    shallow(<MessageList />)
  });

  it('should sort the messages in ascending order', () => {
    const messages = [
      {
        id: '3',
        message: '3',
        timestamp: '2017-02-09T04:27:38Z'
      },
      {
        id: '1',
        message: '1',
        timestamp: '2016-06-03T20:24:29Z'
      },
      {
        id: '2',
        message: '2',
        timestamp: '2016-11-09T05:04:58Z'
      },
    ];

    const wrapper = shallow(<MessageList messages={messages} />)
    
    expect(wrapper.find(Message).at(0).prop('id')).toBe('1');
    expect(wrapper.find(Message).at(1).prop('id')).toBe('2');
    expect(wrapper.find(Message).at(2).prop('id')).toBe('3');
  });

  it('should render <Message> with the right props', () => {
    const messages = [
      {
        id: '1',
        userId: 'user1',
        message: 'message 1',
        timestamp: '2016-06-03T20:24:29Z'
      },
    ];

    const members = {
      user1: {
        name: 'User 1',
      }
    }

    const wrapper = shallow(<MessageList messages={messages} members={members} />)
    
    expect(wrapper.find(Message).at(0).prop('id')).toBe('1');
    expect(wrapper.find(Message).at(0).prop('userId')).toBe('user1');
    expect(wrapper.find(Message).at(0).prop('message')).toBe('message 1');
    expect(wrapper.find(Message).at(0).prop('timestamp')).toBe('2016-06-03T20:24:29Z');
    expect(wrapper.find(Message).at(0).prop('member')).toEqual(members.user1);
  });
});

