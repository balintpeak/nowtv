import React from 'react';
import ReactDOM from 'react-dom';
import Message from './Message';
import { shallow } from 'enzyme';

describe('Message', () => {
  it('should render without crashing', () => {
    shallow(<Message />)
  });

  it('should render an image, date and message', () => {
    const props = {
      id: '1',
      userId: 'user1',
      message: 'message 1',
      timestamp: '2016-06-03T20:24:29Z',
      member: {
        avatar: 'avatar.jpg',
        email: 'user@user.com',
      }
    };

    const wrapper = shallow(<Message {...props} />)

    expect(wrapper.find('li').find('img').prop('src')).toBe('avatar.jpg');
    expect(wrapper.find('li').find('span').text()).toBe('June 3rd 2016, 9:24:29 pm');
    expect(wrapper.find('div').find('p').at(0).text()).toBe('message 1');
    expect(wrapper.find('div').find('p').at(1).text()).toBe('');
  });

  it('should show the email on hover', () => {
    const props = {
      message: 'message 1',
      member: {
        email: 'user@user.com',
      }
    };

    const wrapper = shallow(<Message {...props} />)
    expect(wrapper.find('div').find('p').at(1).text()).toBe('');

    wrapper.find('div').find('p').at(0).simulate('mouseEnter');
    expect(wrapper.find('div').find('p').at(1).text()).toBe('user@user.com');

    wrapper.find('div').find('p').at(0).simulate('mouseLeave');
    expect(wrapper.find('div').find('p').at(1).text()).toBe('');
  });
});

