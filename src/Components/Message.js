import React, { PureComponent } from 'react'
import moment from 'moment'

class Message extends PureComponent {
  constructor() {
    super();

    this.state = { showEmail: false };
    this.toggleShowEmail = this.toggleShowEmail.bind(this);
  }

  toggleShowEmail(show) {
    this.setState({
      showEmail: show
    });
  }

  render() {
    const { message = '', member = {}, timestamp = '' } = this.props;
    const { showEmail } = this.state;
    const formattedDateTime = moment(timestamp).format('MMMM Do YYYY, h:mm:ss a');

    return (
      <li style={{ listStyle: 'none', marginBottom: '10px', display: 'flex' }}>
        <img src={member.avatar} alt="member avatar" style={{ marginRight: '10px' }} />
        <span style={{fontSize: '13px', display: 'inline-block', width: '220px', minWidth: '220px'}}>{formattedDateTime}</span>
        <div>
          <p onMouseEnter={() => this.toggleShowEmail(true)} onMouseLeave={() => this.toggleShowEmail(false)}>{message}</p>
          {showEmail && (
            <p>{member.email}</p>
          )}
        </div>
      </li>
    )
  }
}

export default Message
