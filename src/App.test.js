import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { mount } from 'enzyme';

import MessageList from './Components/MessageList';
jest.mock('./Components/MessageList');
MessageList.mockImplementation(() => <div />);

describe('App', () => {
  it('should render without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App.WrappedComponent getChatLog={() => {}} getChatMembers={() => {}} />, div);
  });

  it('should call getChatLog and getChatMembers when mounted', () => {
    const getChatLogMock = jest.fn();
    const getChatMembersMock = jest.fn();

    mount(<App.WrappedComponent getChatLog={getChatLogMock} getChatMembers={getChatMembersMock} />);

    expect(getChatLogMock).toHaveBeenCalledTimes(1);
    expect(getChatMembersMock).toHaveBeenCalledTimes(1);
  });
});

