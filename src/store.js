import { createStore, combineReducers, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';

export const reducer = {
  messages: (state = [], action = {}) => {
    switch (action.type) {
      case 'MESSAGES_LOADING_FULFILLED':
        return action.payload;

      default:
        return state;
    }
  },
  
  members: (state = {}, action = {}) => {
    switch (action.type) {
      case 'MEMBERS_LOADING_FULFILLED':
        let newState = {};
        if (Array.isArray(action.payload)) {
          newState = action.payload.reduce((membersById, member) => ({ ...membersById, [member.id]: member }), {});
        }
        return newState;
      
      default:
        return state;
    }
  }
}


export const store = createStore(combineReducers({ ...reducer }), {}, applyMiddleware(
  promiseMiddleware()
));
