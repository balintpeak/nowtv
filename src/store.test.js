import { reducer } from './store';

describe('messages', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'mockState' };
    const mockAction = { type: 'mockAction' };
    const result = reducer.messages(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mockAction' };
    const result = reducer.messages(undefined, mockAction);
    expect(result).toEqual([]);
  });

  it('should set messages in the store', () => {
    const messages = [{
      id: 'cd445e6d-e514-424f-ba8f-16ec842002c6',
      userId: 'fe27b760-a915-475c-80bb-7cdf14cc6ef3',
      message: 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
      timestamp: '2017-02-09T04:27:38Z'
    }]

    const newState = reducer.messages([], { type: 'MESSAGES_LOADING_FULFILLED', payload: messages });

    expect(newState).toEqual(messages);
  });
})

describe('members', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'mockState' };
    const mockAction = { type: 'mockAction' };
    const result = reducer.members(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mockAction' };
    const result = reducer.members(undefined, mockAction);
    expect(result).toEqual({});
  });

  it('should set members in the store', () => {
    const members = [{
      id: 'e837c9f5-247f-445f-bcc3-7d434348336b',
      firstName: 'Martin',
      lastName: 'Bradley',
      email: 'mbradley0@google.it',
      avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff',
      ip: '166.124.172.160'
    }]

    const newState = reducer.members([], { type: 'MEMBERS_LOADING_FULFILLED', payload: members });

    expect(newState).toEqual({ 'e837c9f5-247f-445f-bcc3-7d434348336b': members[0] });
  });
})
