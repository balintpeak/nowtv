import { getChatLog, getChatMembers } from './service';

jest.mock('./data');
import { getMessages, getMembers } from './data';

getMessages.mockImplementation(() => 'messages');
getMembers.mockImplementation(() => 'members');

describe('service', () => {
  describe('getChatLog', () => {
    it('should return an action with MESSAGES_LOADING action type and the fetched data', () => {
      const expected = {
        type: 'MESSAGES_LOADING',
        payload: 'messages',
      };

      expect(getChatLog()).toEqual(expected)
    })
  })

  describe('getChatMembers', () => {
    it('should return an action with MESSAGES_LOADING action type and the fetched data', () => {
      const expected = {
        type: 'MEMBERS_LOADING',
        payload: 'members',
      };

      expect(getChatMembers()).toEqual(expected)
    })
  })
});
